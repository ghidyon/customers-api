﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Models
{
    public class Account
    {
        [Column("AccountId")]
        public Guid Id { get; set; }
        
        [MinLength(10)]
        [MaxLength(10)]
        public string AccountNumber { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public decimal AccountBalance { get; set; }
    }
}
