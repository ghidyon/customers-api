﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Entities.Configuration
{
    public class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasData
            (
                new Account
                {
                    Id = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"),
                    AccountNumber = "1005345781",
                    AccountBalance = 30000.99m
                },
                
                new Account
                {
                    Id = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"),
                    AccountNumber = "1005345782",
                    AccountBalance = 50000.99m
                }
            );
        }
    }
}
