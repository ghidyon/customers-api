﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
//using Micro

namespace Entities.Configuration
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasData
            (
                new Customer
                {
                    Id = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"),
                    Name = "Gideon Akunana",
                    Gender = "Male",
                    Email = "ghidyon@test.com",
                    Address = "Tenece House, Enugu",
                    DateOfBirth = new DateTime(1991, 11, 11),
                    AccountId = new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870")
                },

                new Customer
                {
                    Id = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"),
                    Name = "Chidiebube Onah",
                    Email = "ebube@test.com",
                    Gender = "Male",
                    Address = "Tenece House, Enugu",
                    DateOfBirth = new DateTime(1990, 10, 10),
                    AccountId = new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3")
                }
            );
        }
    }
}
