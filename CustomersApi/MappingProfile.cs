﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;
using Entities.DataTransferObjects;

namespace CustomersApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDto>()
                .ForMember(c => c.AccountBalance, option => option.MapFrom(x => x.Account.AccountBalance))
                .ForMember(c => c.AccountNumber, option => option.MapFrom(x => x.Account.AccountNumber));
        }
    }
}
