﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomersApi.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "AccountId", "AccountBalance", "AccountNumber" },
                values: new object[] { new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"), 30000.99m, "1005345781" });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "AccountId", "AccountBalance", "AccountNumber" },
                values: new object[] { new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"), 50000.99m, "1005345782" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "AccountId", "Address", "DateOfBirth", "Email", "Gender", "Name" },
                values: new object[] { new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"), new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"), "Tenece House, Enugu", new DateTime(1991, 11, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "ghidyon@test.com", "Male", "Gideon Akunana" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerId", "AccountId", "Address", "DateOfBirth", "Email", "Gender", "Name" },
                values: new object[] { new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"), new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"), "Tenece House, Enugu", new DateTime(1990, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "ebube@test.com", "Male", "Chidiebube Onah" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "CustomerId",
                keyValue: new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "CustomerId",
                keyValue: new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"));

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "AccountId",
                keyValue: new Guid("3d490a70-94ce-4d15-9494-5248280c2ce3"));

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "AccountId",
                keyValue: new Guid("c9d4c053-49b6-410c-bc78-2d54a9991870"));

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Customers");
        }
    }
}
